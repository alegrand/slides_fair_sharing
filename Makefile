all: slides.pdf

LATEX=lualatex -shell-escape

%.tex: %.org
	emacs --batch --eval "(add-to-list 'load-path \"./\")" --eval "(add-to-list 'load-path \"~/.emacs.d/elpa/org-ref-20200107.1307/\")" --eval "(setq enable-local-eval t)" --eval "(setq enable-local-variables t)" $<  --funcall org-beamer-export-to-latex

%.pdf: %.tex biblio.bib
	$(LATEX) $<
	bibtex `basename $< .tex`
	$(LATEX) $<
	$(LATEX) $<


